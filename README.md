# COGS 108 Classwork

This repository contains work from the [Data Science in Practice class at UCSD](https://github.com/COGS108/Overview)

## Contents
Assignment 1: [Setting Up](A1/A1_SettingUp.ipynb)

Assignment 2: [Data Exploration](A2/A2_DataExploration.ipynb)

Assignment 3: [Data Privacy](A3/A3_DataPrivacy.ipynb)

Assignment 4: [Data Analysis](A4/A4_DataAnalysis.ipynb)

Assignment 5: [Natural Language Processing](A5/A5_NaturalLanguageProcessing.ipynb)

Final Project: [Inequalities and Titanic Survival Rates](FinalProject_group.ipynb)
